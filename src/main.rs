use engine::{Game, components, systems};
use engine::vertices::UVVertex;

mod test_system;

fn main() {
    let mut game = Game::new("Game");
    game.init_color_vertex_shader();

    let vertices = vec![
        UVVertex::new(0.0, 0.5, 0.0, 0.0, 0.0),
        UVVertex::new(0.5, -0.5, 0.0, 1.0, 0.0),
        UVVertex::new(-0.5, -0.5, 0.0, 0.0, 1.0),
    ];

    let gl = game.get_gl();

    game.init_camera_components();
    game.get_mut_component_manager().register_component::<components::Camera>();
    game.get_mut_component_manager().register_component::<components::Object>();
    game.get_mut_component_manager().register_component::<components::Transform>();
    game.get_mut_component_manager().register_component::<components::Time>();
    game.get_mut_component_manager().register_component::<components::Texture>();

    game.init_uv_vertex_shader();

    let camera = game.create_entity("Camera");
    game.get_mut_component_manager().add_component(camera, components::Camera::new(2.0, 0.0, 2.0, true));
    let object = game.create_entity("Object");
    game.get_mut_component_manager().add_component(object, components::Object::new_uv(&vertices, &gl));
    game.get_mut_component_manager().add_component(object, components::Transform::new());
    game.get_mut_component_manager().add_component(object, components::Time::new());
    game.get_mut_component_manager().add_component(object, components::Texture::new("texture-test.png", &gl));

    game.get_mut_system_manager().create_system(systems::SysCameraUpdate{});
    game.get_mut_system_manager().add_system_to_main_thread(systems::SysUVVertexRenderer{});
    game.get_mut_system_manager().create_system(systems::SysTransform{});
    game.get_mut_system_manager().create_system(systems::SysTime{});
    game.get_mut_system_manager().create_system(test_system::TestSystem{});

    game.run();
}
