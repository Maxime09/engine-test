use engine::{
    ecs::System,
    components::{Transform, Time, Input},
    render_lib::{sdl::keyboard::Keycode, na::Vector3},
};
use std::sync::{Arc, Mutex};

pub struct TestSystem;

impl System for TestSystem{
    fn run(&mut self, entities: &engine::ecs::EntityManager, components: Arc<Mutex<&mut engine::ecs::ComponentManager>>) {
        let lock = &mut (*components.lock().unwrap());
        for id in entities.comp_pair_iter::<Transform, Time>(*lock){
            let delta_time = lock.get_mut_component::<Time>(id).unwrap().get_delta_time();
            let transform = lock.get_mut_component::<Transform>(id).unwrap();
            transform.rotation += Vector3::new(90.0, 45.0, 175.0) * delta_time;
        }
        for id in entities.comp_iter::<Input>(*lock){
            let input_component = lock.get_mut_component::<Input>(id).unwrap();
            if input_component.get_key_down(Keycode::A){
                println!("Key A pressed");
            }
            if input_component.get_key_up(Keycode::Z){
                println!("Key Z released");
            }
            if input_component.get_key(Keycode::E){
                println!("Key E in use");
            }
        }
    }
}
